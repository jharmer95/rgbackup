#!/usr/bin/python3

import sys, getopt, os, multiprocessing, hashlib, json, datetime, shutil
from subprocess import call, Popen, PIPE

maxThreads = multiprocessing.cpu_count()
numThreads = maxThreads - 2
tempFile = "rgBackup_files.txt"
csvFile = "rgBackup_files.csv"
tempFolder = "rgBackup"
hashName = "NONE"
backupType = "FULL"
refBackup = "NONE"

def main():
    global numThreads
    global tempFile
    global hashName
    global backupType
    global refBackup
    incList = []
    excList = []
    activeDir = os.getcwd()
    outFile = os.getcwd() + "/rgbackup.tar"
    compressLevel = 6
    typeSelected = False

    if (numThreads < 1):
        numThreads = 1

    try:
        opts, loc = getopt.getopt(sys.argv[1:], 'I:E:h:j:z:H:o:f:i:s:', ['include=', 'exclude=', 'help', 'numthreads=', 'ziplevel=', 'hash=', 'output=', 'full', 'incremental', 'reference='])
    except getopt.GetoptError:
        printUsage()
        sys.exit(2)

    varCount = len(loc)

    if varCount > 2:
        printUsage()
        sys.exit(1)
    if varCount >= 1:
        activeDir = loc[0]
    if varCount == 2:
        outFile = loc[1]

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            printUsage()
            sys.exit(0)
        elif opt in ('-I', '--include'):
            if len(excList) > 0:
                printUsage()
                sys.exit(1)
            strs = arg.split(',')
            for s in strs:
                incList.append(s)
        elif opt in ('-E', '--exclude'):
            if len(incList) > 0:
                printUsage()
                sys.exit(1)
            strs = arg.split(',')
            for s in strs:
                excList.append(s)
        elif opt in ('-j', '--numthreads'):
            numThreads = int(arg)
            if (numThreads) > maxThreads:
                sys.stderr.write("Max number of threads exceeded!\n(" + str(numThreads) + " > " + str(maxThreads) + ")")
                sys.exit(1)
            elif (numThreads < 1):
                sys.stderr.write("Must run on at least one thread!")
                sys.exit(1)
        elif opt in ('-z', '--ziplevel'):
            compressLevel = int(arg)
            if (compressLevel < 1 or compressLevel > 9):
                sys.stderr.write("Compression level must be between 1 and 9!")
                sys.exit(1)
        elif opt in ('H', '--hash'):
            hashName = arg
        elif opt in ('o', '--output'):
            outFile = arg
        elif opt in ('f', '--full'):
            if (typeSelected):
                printUsage()
                sys.exit(1)
            backupType = "FULL"
            typeSelected = True
        elif opt in ('i', '--incremental'):
            if (typeSelected):
                printUsage()
                sys.exit(1)
            backupType = "INCREMENTAL"
            typeSelected = True
        elif opt in ('s', '--reference'):
            refBackup = arg

    runBackup(include=incList, exclude=excList, directory=activeDir, outfile=outFile, zipLevel=compressLevel)
    shutil.rmtree(tempFolder, ignore_errors=True)
    print("Done.")

def runBackup(include=[], exclude=[], directory=os.getcwd(), outfile=os.getcwd()+"/backup.tar", zipLevel=6):
    global tempFolder
    global tempFile
    global csvFile
    global hashName
    global backupType
    if os.path.isdir(tempFolder) == False:
        os.mkdir(tempFolder)
    tempFile = tempFolder + "/" + tempFile
    csvFile = tempFolder + "/" + csvFile
    getFileList(include=include, exclude=exclude, directory=directory)
    getFileSizes()
    getHash(hashName)
    if (backupType == "INCREMENTAL"):
        getDiff()
    createZip(compressLevel=zipLevel)
    createTar(outfile)

def getFileList(include=[], exclude=[], directory=os.getcwd()):
    global tempFile
    global tempFolder
    f = open(tempFile, "wb")
    f = open(tempFile, "ab")
    cmd = ["rg", directory, "-j" + str(numThreads), "--files", "--hidden", "-g", "!" + tempFolder + "/*"]

    if (len(include) < 1 and len(exclude) >= 1):
        for d in exclude:
            if d.endswith('/'):
                d = d[:-1]
            cmd.append("-g")
            cmd.append("!" + d + "/*")
        call(cmd, stdout=f)
    elif (len(include) >= 1 and len(exclude) < 1):
        for d in include:
            call(["rg", d, "-j" + str(numThreads), "--files", "--hidden", "-g", "!" + tempFolder + "/*"], stdout=f)
    elif (len(include) < 1 and len(exclude) < 1):
        call(cmd, stdout=f)

    f.close()

def createZip(outfile=tempFolder + "/backup.tar.gz", compressLevel=6):
    global numThreads
    global tempFile
    DEVNULL = open(os.devnull, 'w')
    tar_filename = outfile[:-3]
    tar_cmd = ["tar", "--xattrs", "-cf", tar_filename, "--exclude=" + outfile, "-T", tempFile]
    call(tar_cmd, stderr=DEVNULL)
    pigz_cmd = ["pigz", "-f", "-" + str(compressLevel), "-p", str(numThreads), tar_filename]
    call(pigz_cmd)
    DEVNULL.close()

def getFileSizes():
    global tempFile
    global csvFile
    ifile = open(tempFile, "rb")
    ofile = open(csvFile, "wb")

    cmd = ["xargs", "stat", "-c", "%n,%s"]
    call(cmd, stdout=ofile, stdin=ifile)
    ifile.close()
    ofile.close()

def getHash(algo):
    global tempFile
    global csvFile
    hashList = []
    if algo == "NONE":
        return

    fileList = []
    with open(tempFile, "r") as txtfile:
        for line in txtfile:
            line = line.rstrip('\n')
            fileList.append(line)

    for targetfile in fileList:
        hashList.append(doHash(algo=algo, filename=targetfile))

    i = 0
    with open(csvFile, "r") as istr:
        with open(csvFile + "new", "w") as ostr:
            for line in istr:
                line = line.rstrip('\n')
                line += "," + hashList[i] + '\n'
                ostr.write(line)
                i += 1

    os.remove(csvFile)
    os.rename(csvFile + "new", csvFile)

def doHash(algo, filename):
    BLOCKSIZE = 65536
    if algo == "MD5":
        hasher = hashlib.md5()
    elif algo == "SHA1":
        hasher = hashlib.sha1()
    elif algo == "SHA256":
        hasher = hashlib.sha256()
    elif algo == "BLAKE2B":
        hasher = hashlib.blake2b(digest_size=32)
    with open(filename, "rb") as f:
        while True:
            data = f.read(BLOCKSIZE)
            if not data:
                break
            hasher.update(data)
    return hasher.hexdigest()

def createTar(outfile=os.getcwd() + "/backup.tar"):
    global hashName
    global tempFolder
    global csvFile
    global backupType
    data = {
        "backup": {
            "name": "backup",
            "time": str(datetime.datetime.now()),
            "backup type": backupType,
            "csv_file": csvFile,
            "hash": hashName
        }
    }
    with open(tempFolder + "/metafile.json", "w") as json_file:
        json.dump(data, json_file, indent=4)
    os.remove(tempFile)
    tar_cmd = ["tar", "-cf", outfile, tempFolder]
    call(tar_cmd)

def getDiff():
    global csvFile

def printUsage():
    print('usage: rgBackup [OPTIONS] [WORKING DIR] [OUTPUT FILE]\n\nOPTIONS:\n\t"-I", "--include" [DIRS]\n\t\tDIRS : directories to include, separated by commas\n\t"-E", "--exclude" [DIRS]\n\t\tDIRS : directories to exclude, separated by commas')

if __name__ == "__main__":
    main()