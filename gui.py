import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio
from gi.repository import GObject
from subprocess import call
import sys

dirList = list()

def updateLB():
    listbox = builder.get_object("DirectoryLB")
    listbox.forall(listbox.remove)
    for dir in dirList:
        listbox.add(ListBoxRowWithData(dir))
        listbox.show_all()

def rg(dlist, exclude):
    f = open("/home/jackson/files4.txt", "wb")
    f = open("/home/jackson/files4.txt", "ab")
    if exclude == True:
        l = ["pkexec", "/home/jackson/Code/Python/rgBackup/rg.sh"]
        for d in dlist:
            l.append("-g")
            l.append("!" + d + "/*")
        call(l)
    elif exclude == False:
        for d in dlist:
            call(["rg", d, "--files", "--hidden"], stdout=f)

    f.close()
    f = open("/home/jackson/files4.txt", "rb")
    fList = f.readlines()
    fBuf = builder.get_object("FileListTB").get_buffer()
    fStr = ""
    for fl in fList:
        fStr += fl
    fBuf.set_text(fStr)
class ListBoxRowWithData(Gtk.ListBoxRow):
    def __init__(self, data):
        super(Gtk.ListBoxRow, self).__init__()
        self.data = data
        self.add(Gtk.Label(data))

class Handler(Gtk.Window):

    def onDestroy(self, *args):
        Gtk.main_quit()

    def onButtonPressed(self, button):
        dialog = Gtk.FileChooserDialog("Please choose a folder", self, Gtk.FileChooserAction.SELECT_FOLDER,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,"Select", Gtk.ResponseType.ACCEPT))
        dialog.set_default_size(640, 480)

        response = dialog.run()
        if response == Gtk.ResponseType.ACCEPT:
            print("Folder selected: " + dialog.get_filename())
            dirList.append(dialog.get_filename())
            updateLB()
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()

    def okPressed(self, button):
        exclude = builder.get_object("ExcludeRadioBtn").get_active()
        rg(dirList, exclude)

builder = Gtk.Builder()
builder.add_from_file("Glade/rgBackup.glade")
builder.connect_signals(Handler())

window = builder.get_object("window1")
window.show_all()

Gtk.main()