#!/usr/bin/python3

import sys, getopt, os, multiprocessing, hashlib, json, datetime, shutil
from subprocess import call, Popen, PIPE

maxThreads = multiprocessing.cpu_count()
numThreads = maxThreads - 2

def main():
    global numThreads
    global maxThreads
    activeFile = ""
    outDir = "rgBackup_extracted"
    verify = False

    try:
        opts, loc = getopt.getopt(sys.argv[1:], 'h:j:V:', ['help', 'numthreads=', 'verify'])
    except getopt.GetoptError:
        printUsage()
        sys.exit(2)

    varCount = len(loc)

    if varCount > 2 or varCount < 1:
        printUsage()
        sys.exit(1)
    if varCount >= 1:
        activeFile = loc[0]
    if varCount == 2:
        outDir = loc[1]

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            printUsage()
            sys.exit(0)
        elif opt in ('-j', '--numthreads'):
            numThreads = int(arg)
            if (numThreads) > maxThreads:
                sys.stderr.write("Max number of threads exceeded!\n(" + str(numThreads) + " > " + str(maxThreads) + ")")
                sys.exit(1)
            elif (numThreads < 1):
                sys.stderr.write("Must run on at least one thread!")
                sys.exit(1)
        elif opt in ('-V', '--verify'):
            verify = True

    runRestore(filename=activeFile, output=outDir)
    if verify == True:
        runVerify(filename=activeFile, dir=outDir)
    shutil.rmtree(outDir + "rgBackup")
    print("Done")

def runRestore(filename, output):
    DEVNULL = open(os.devnull, 'w')
    if os.path.isdir(output) == False:
        os.mkdir(output)
    tar_cmd = ["tar", "-xvf", filename, "-C", output]
    call(tar_cmd, stderr=DEVNULL)
    f = open(output + "rgBackup/backup.tar", "wb")
    pigz_cmd = ["pigz", "-dc", "-p", str(numThreads), output + "rgBackup/backup.tar.gz"]
    call(pigz_cmd, stderr=DEVNULL, stdout=f)
    f.close()
    tar_cmd2 = ["tar", "-xvf", output + "rgBackup/backup.tar", "-C", output]
    call(tar_cmd2, stderr=DEVNULL)

    DEVNULL.close()

def runVerify(filename, dir):
    print("verified")

def printUsage():
    print("Ya did it wrong!")

if __name__ == "__main__":
    main()